#!/bin/bash

rtt=0.5

rm -rf tmp
mkdir tmp
ln -s $(readlink -f $1) tmp/snap12-3D.dat
cp ~/COCOA/*.py tmp/
cp -R ~/COCOA/sim2obs tmp/
cd tmp
python projection.py
echo rtt=$rtt > param_rtt.tmp
tail -n 1 param_rtt.py >> param_rtt.tmp
mv param_rtt.tmp param_rtt.py
python observation.py
~/ds9 fits-files/0.0.fits -zoom to fit -height 512 -width 512 -scale log -colorbar no -saveimage jpeg 0.0.jpeg -exit
mv 0.0.jpeg ..
cd ..
rm -rf tmp
