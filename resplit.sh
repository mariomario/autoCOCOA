#!/bin/bash
#
#This script makes a train and a test folder with subfolders nonhost_images and host_images
#It then makes symbolic links from there to images in nonhost_images and in host_images folders
#$1 images from folder nonhost_images are linked from train/nonhost_images and the rest from test/nonhost_images
#$2 images from folder host_images are linked from train/host_images and the rest from test/host_images
#In other words
#$1 should be number of nonhost_images images to include in train
#$2 number of host_images images to include in train
#$1 should be < number of files in nonhost_images
#$2 should be < number of files in host_images
# e.g.
# ./split.sh 1190 10710 #puts 70% in train, 30% in test

rm -rf train test *d.tmp
mkdir train
mkdir train/nonhost_images
mkdir train/host_images
mkdir test
mkdir test/nonhost_images
mkdir test/host_images

ls nonhost_images | shuf > nonhost_images.tmp
ls host_images | shuf > host_images.tmp

#creating train folders
for nome in $(cat nonhost_images.tmp | head -n $1)
do
    ln -s $(pwd)/nonhost_images/$nome $(pwd)/train/nonhost_images/$nome
done
for nome in $(cat host_images.tmp | head -n $2)
do 
    ln -s $(pwd)/host_images/$nome $(pwd)/train/host_images/$nome
done

#creating test folders with the remaining files
a=$(echo "$(ls nonhost_images | wc --l) - $1" | bc)
b=$(echo "$(ls host_images | wc --l) - $2" | bc)

for nome in $(cat nonhost_images.tmp | tail -n $a)
do
    ln -s $(pwd)/nonhost_images/$nome $(pwd)/test/nonhost_images/$nome
done
for nome in $(cat host_images.tmp | tail -n $b)
do 
    ln -s $(pwd)/host_images/$nome $(pwd)/test/host_images/$nome
done



rm -rf *d.tmp
