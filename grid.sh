#!/bin/bash
#testing different values of convolutional layers / dense layers dropout

#CONVDROP
#DENSDROP

mkdir -p dropout_grid_outputs
for CONVDROP in $(seq 0.0 0.1 0.9)
do
    for DENSDROP in $(seq 0.0 0.1 0.9)
    do
        echo "Training with convolutional dropout $CONVDROP and dense $DENSDROP"
        cat train.template | sed s/DENSDROP/$DENSDROP/g | sed s/CONVDROP/$CONVDROP/g > thistrain.py
        python thistrain.py > dropout_grid_outputs/output_"$CONVDROP"_"$DENSDROP"
    done
done
