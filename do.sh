#!/bin/bash

for snap in $(cat  /data5MM/pasquato/all/list | shuf)
do
    ./generate.sh /data5MM/pasquato/all/$snap/snap12-3D.dat
    mkdir -p $snap
    mv 0.0.jpeg $snap
done
