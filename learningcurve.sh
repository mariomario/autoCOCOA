#!/bin/bash

echo "loss,acc,val_loss,val_acc" >> $1.csv
grep val_acc dropout_grid_outputs/$1 | sed s/-/:/g | cut -f 4,6,8,10 -d ":" | sed s/' : '/,/g >> $1.csv

