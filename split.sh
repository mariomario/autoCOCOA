#!/bin/bash

cat holes | grep "Y" | cut -b 23- | cut -d " " -f 1 | sed s/'\/snap12-3D.dat'//g > hosts
cat holes | grep "N" | cut -b 23- | cut -d " " -f 1 | sed s/'\/snap12-3D.dat'//g > nonhosts

mkdir host_images
mkdir nonhost_images

for nome in $(cat hosts)
do
     cognome=$(echo $nome | sed s/"\/"/"_"/g)
     cp $nome/0.0.jpeg host_images/$cognome.jpeg
done

for nome in $(cat nonhosts)
do
     cognome=$(echo $nome | sed s/"\/"/"_"/g)
     cp $nome/0.0.jpeg nonhost_images/$cognome.jpeg
done

