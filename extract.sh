#!/bin/bash

echo conv_dropout dense_dropout val_acc
for nome in  $(ls dropout_grid_outputs/)
do
    echo $nome | tr -d "\n" | sed s/'_'/' '/g | sed s/'output'//g | sed s/"^ "//g
    grep "val_acc" dropout_grid_outputs/$nome | tail -n 1 | cut -d ":" -f 5-
done
